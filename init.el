(package-initialize)
(org-babel-load-file "~/.emacs.d/emacs.org")
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector
   ["#FCFBF9" "#8F5652" "#747B4D" "#886A44" "#556995" "#83577D" "#477A7B" "#605A52"])
 '(custom-safe-themes
   '("d268b67e0935b9ebc427cad88ded41e875abfcc27abd409726a92e55459e0d01" "246a9596178bb806c5f41e5b571546bb6e0f4bd41a9da0df5dfbca7ec6e2250c" "a7b20039f50e839626f8d6aa96df62afebb56a5bbd1192f557cb2efb5fcfb662" "3d47380bf5aa650e7b8e049e7ae54cdada54d0637e7bac39e4cc6afb44e8463b" "f91395598d4cb3e2ae6a2db8527ceb83fed79dbaf007f435de3e91e5bda485fb" "4f1d2476c290eaa5d9ab9d13b60f2c0f1c8fa7703596fa91b235db7f99a9441b" "93ed23c504b202cf96ee591138b0012c295338f38046a1f3c14522d4a64d7308" "4a8d4375d90a7051115db94ed40e9abb2c0766e80e228ecad60e06b3b397acab" "c4bdbbd52c8e07112d1bfd00fee22bf0f25e727e95623ecb20c4fa098b74c1bd" "e074be1c799b509f52870ee596a5977b519f6d269455b84ed998666cf6fc802a" "f2927d7d87e8207fa9a0a003c0f222d45c948845de162c885bf6ad2a255babfd" "08a27c4cde8fcbb2869d71fdc9fa47ab7e4d31c27d40d59bf05729c4640ce834" "f4876796ef5ee9c82b125a096a590c9891cec31320569fc6ff602ff99ed73dca" "990e24b406787568c592db2b853aa65ecc2dcd08146c0d22293259d400174e37" "8f5a7a9a3c510ef9cbb88e600c0b4c53cdcdb502cfe3eb50040b7e13c6f4e78e" "fce3524887a0994f8b9b047aef9cc4cc017c5a93a5fb1f84d300391fba313743" default))
 '(exwm-floating-border-color "#CDCBC7")
 '(fci-rule-color "#9E9A95")
 '(highlight-tail-colors
   ((("#eeeee7" "#eff3ef" "green")
	 . 0)
	(("#e9eeec" "#f3f7f7" "brightgreen")
	 . 20)))
 '(jdee-db-active-breakpoint-face-colors (cons "#fcfbf9" "#7686A9"))
 '(jdee-db-requested-breakpoint-face-colors (cons "#fcfbf9" "#747B4D"))
 '(jdee-db-spec-breakpoint-face-colors (cons "#fcfbf9" "#9E9A95"))
 '(objed-cursor-color "#8F5652")
 '(org-agenda-files
   '("~/Dropbox/org/tasks.org" "~/Dropbox/org/appointments.org" "~/Dropbox/org/dates.org"))
 '(org-hide-emphasis-markers nil)
 '(package-selected-packages
   '(hide-mode-line flycheck company markdown-mode neotree NeoTree projectile magit org-ac org-plus-contrib ivy which-key doom-modeline smartparens diminish use-package doom-themes evil))
 '(pdf-view-midnight-colors (cons "#605A52" "#FCFBF9"))
 '(rustic-ansi-faces
   ["#FCFBF9" "#8F5652" "#747B4D" "#886A44" "#556995" "#83577D" "#477A7B" "#605A52"])
 '(vc-annotate-background "#FCFBF9")
 '(vc-annotate-color-map
   (list
	(cons 20 "#747B4D")
	(cons 40 "#7a7549")
	(cons 60 "#816f47")
	(cons 80 "#886A44")
	(cons 100 "#886a44")
	(cons 120 "#886a44")
	(cons 140 "#886A44")
	(cons 160 "#866357")
	(cons 180 "#845d69")
	(cons 200 "#83577D")
	(cons 220 "#87566e")
	(cons 240 "#8b5660")
	(cons 260 "#8F5652")
	(cons 280 "#926762")
	(cons 300 "#967873")
	(cons 320 "#9a8984")
	(cons 340 "#9E9A95")
	(cons 360 "#9E9A95")))
 '(vc-annotate-very-old-color nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
